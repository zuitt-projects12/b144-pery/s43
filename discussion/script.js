let posts = new Array();
let count = 1;

// Add post data.
document.querySelector("#form-add-post").addEventListener("submit", (e) => {
	// This will prevent the page form loading
	e.preventDefault();

	posts.push({
		id: count,
		title: document.querySelector("#txt-title").value,
		body: document.querySelector("#txt-body").value
	}); // end posts.push

	// Increment ID
	count++;

	showPosts(posts);
	alert(`${document.querySelector("#txt-title").value} successfully added.`);
	document.querySelector("#txt-title").value = "";
	document.querySelector("#txt-body").value = "";

}); // end document.querySelector.addEventListener
document.querySelector("#form-edit-post").addEventListener("submit", (e) => {
	// This will prevent the page form loading
	e.preventDefault();

	// posts[document.querySelector("#txt-edit-id").value - 1].title = document.querySelector("#txt-edit-title").value;
	// posts[document.querySelector("#txt-edit-id").value - 1].body = document.querySelector("#txt-edit-body").value;

	for(let i = 0; i < posts.length; i++){
		if(posts[i].id.toString() === document.querySelector("#txt-edit-id").value){
			posts[i].title = document.querySelector("#txt-edit-title").value;
			posts[i].body = document.querySelector("#txt-edit-body").value;
			showPosts(posts);
			alert(`${posts[i].title} successfully updated`);
			document.querySelector("#txt-edit-title").value = "";
			document.querySelector("#txt-edit-body").value = "";
			break;
		}
	}


	// showPosts(posts);
	// alert(`${document.querySelector("#txt-edit-title").value} successfully updated.`);
	// document.querySelector("#txt-edit-id").value = "";
	// document.querySelector("#txt-edit-title").value = "";
	// document.querySelector("#txt-edit-body").value = "";

}); // end document.querySelector.addEventListener

const editPost = (id) => {
	let title = document.querySelector(`#post-title-${id}`).innerHTML;
	let body = document.querySelector(`#post-body-${id}`).innerHTML;

	document.querySelector('#txt-edit-id').value = id;
	document.querySelector('#txt-edit-title').value = title;
	document.querySelector('#txt-edit-body').value = body;

};

// Show posts
const showPosts = (posts) => {
	let postEntries = "";
	posts.forEach(post => {
		postEntries += `
			<div id="post-${post.id}" class="my-3">
				<h3 id="post-title-${post.id}">${post.title}</h3>
				<p id="post-body-${post.id}">${post.body}</p>
				<button onclick="editPost('${post.id}')" class="btn btn-warning">Edit</button>
				<button onclick="deletePost('${post.id}')" class="btn btn-danger">Delete</button>
			</div>
		`;
	}); // end posts.forEach

	document.querySelector("#div-post-entries").innerHTML = postEntries;
}; // end const showPosts

const deletePost = (postId) => {
	// let title = document.querySelector(`#post-title-${postId}`).value;
	posts = posts.filter(post => post.id.toString() !== postId);
	alert(`The movie has been deleted`);
	showPosts(posts);
};
